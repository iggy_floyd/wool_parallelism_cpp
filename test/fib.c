/** \page
 * !!! WoolFibonacciStandAlone
 * 
 * Example of the fibonacci calculation using wool api 
 */
#include "wool.h"
#include <stdio.h>
#include <stdlib.h>

TASK_1( int, pfib, int, n )
{
   if( n < 2 ) {
      return n;
   } else {
      int m,k;
      SPAWN( pfib, n-1 );
      k = CALL( pfib, n-2 );
      m = SYNC( pfib );
      return m+k;
   }
}

/**
 *  gcc -lpthread -O3 -o fib fib.c ../lib/libwool_parallelism_cpp.a `(cd ..; ./config.sh --inc)`
 * 
*/

int main( int argc, char **argv )
{
  int n,m;
  argc = wool_init( argc, argv );
  n = atoi( argv[ 1 ] );
  m = CALL( pfib, n );
  printf( "%d\n", m );
  wool_fini( );
  return 0;

}





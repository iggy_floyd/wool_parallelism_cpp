/** \page
 * !!! WoolLoopStandAlone
 * 
 * This loop represents the Levenshtein algorithm with Wool spawning tasks
 * version 1
 */


#include <vector>
#include <string>
#include <algorithm>
#include "wool.h"
#include <iostream>        
       


LOOP_BODY_4(work, LARGE_BODY, int, i,void * , prevCol,void *,col,void *, s1, void *,s2)
{
  
  std::vector<unsigned int>  &  _prevCol = *(static_cast<std::vector<unsigned int>  *>(prevCol));
  std::vector<unsigned int>  & _col = *(static_cast<std::vector<unsigned int>  *>(col));
  std::string &  _s1 = *(static_cast<std::string *>(s1));
  std::string & _s2 = *(static_cast<std::string *>(s2));
    
  _col[0] = i+1;  
  unsigned int j;
  unsigned int len2 = _s2.size();
  
  for( j=0; j<len2; j++ ) {
      _col[j+1] = std::min( std::min(_prevCol[1 + j] + 1, _col[j] + 1),
                    _prevCol[j] + (_s1[i]==_s2[j] ? 0 : 1) );
  }
  _col.swap(_prevCol);
  
}
      

/**
 *  g++ -lpthread -O3 -o loop loop.c ../lib/libwool_parallelism_cpp.a `(cd ..; ./config.sh --inc)`
 * 
*/


int main( int argc, char **argv )
{
  
  argc = wool_init( argc, argv );  
  std::string str1 = "igor-marfin";
  std::string str2 = "igor1-marfin";
  const size_t len1 = str1.size(), len2 = str2.size();
  
  std::vector<unsigned int> col(len2+1), prevCol(len2+1);
  for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
  
  FOR( work, 0, len1, &prevCol,&col,&str1,&str2);
  

  double distance = prevCol[len2];
  std::cout<<"distance: "<<distance<<"\n";
  if (distance == 0 ) {
      std::cout<<double(distance)<<"\n";
      return 0;
  }
      
  double maxlen = (double) std::max(str1.size(), str2.size());  
  std::cout<<maxlen<<"\n";
  std::cout<<std::min(1.,distance/maxlen)<<"\n";
  wool_fini( );
  
  return 0;
  
}
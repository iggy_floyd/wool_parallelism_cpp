
/** \page
!!! WoolFibonacci 
 * 
 * Example of application of the Wool technology for fibonacci algorithm
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include <chrono>
#include <thread>

using namespace std;

#include "wool.h"

/**
 * TASK_1 is the definition of subroutine to be spawned in parallel several times
 */
TASK_1( int, pfib, int, n )
{
   if( n < 2 ) {
      return n;
   } else {
      int m,k;
      SPAWN( pfib, n-1 );
      k = CALL( pfib, n-2 );
      m = SYNC( pfib );
      return m+k;
   }
}


const std::string  test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");
  
    
    int m, n=40;
    double wall01;
    char* argv[] = { "test", "-p", "4", 0 }; //! <We set up the parameters of the command line to initilaze properly                                                  //! <Wool library
    int   argc   = 2;
    wall01=tools::get_wall_time(); 
    argc = wool_init( argc, argv );      //! <Initialization of the Wool
    m = CALL( pfib, n ); //! <CALL is used to start executing the 'parallel' code 
    wool_fini( );//! <Close of the Wool
    wall01=tools::get_wall_time()-wall01;
    ss<<"result: "<<m<<" time elapsed: "<<wall01;
    
    return ss.str();
}    


#include <boost/python.hpp>

BOOST_PYTHON_MODULE(fib)
{
    using namespace boost::python;
    def("test", test);

}

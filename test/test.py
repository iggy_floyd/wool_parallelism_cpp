#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
/** \page 
!!!  Test suite of the project.
*  
*
*
*  Copyright (c) 2014, <Unister Gmbh, Igor Marfin <igor.marfin@unister.de> >
*  All rights reserved.
*
*
*  Usage: %(scriptName)s
*
*/
/** \dir
!!!  Test suite of the project.
*
*/
'''


from subprocess import Popen,PIPE
import inspect
import sys


sys.path.append('.')



def test_cpp(file,incs="",args=""):

 cmd="python-config --cflags"
 cmd=cmd.split()
 (pyc,err) = Popen(cmd, stdout=PIPE).communicate()
 pyc=pyc.replace("-Wstrict-prototypes","");
 pyc=pyc.replace("\n","");
 cmd="g++  -std=c++0x  -I. %s   -I/usr/local/include 	   %s -fpic %s.cpp -shared -lboost_python %s -o %s.so"%(pyc,incs,file,args,file)
# print cmd
# return 
 cmd=cmd.split()
 (out,err) = Popen(cmd, stdout=PIPE).communicate()
 import importlib
 mymodule = importlib.import_module(file, package=file)
 
 return getattr(mymodule,"test")()

  




def hello():
   """  hello.cpp

   >>> hello()
   'hello, world'
   """
   return test_cpp(str(inspect.stack()[0][3]))
   
def levenshtein():
   r"""  levenshtein.cpp
         encode( '", \\, \\t, \\n' )
   >>> levenshtein()
   distance: 0 time:7.50779e-05
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   cmd_libs=""
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def fib():
   r"""  fib.cpp
         encode( '", \\, \\t, \\n' )
   >>> fib()   
   result: 102334155 time elapsed: 0.287057
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))

def fib_ser():
   r"""  fib_ser.cpp
         encode( '", \\, \\t, \\n' )
   >>> fib_ser()   
   result: 102334155 time elapsed: 0.745857   
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))

def loop_wool():
   r"""  loop_wool.cpp
         encode( '", \\, \\t, \\n' )
   >>> loop_wool()   
   result: 0.52381 time elapsed: 0.00215188   
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   options=" -O4  -ftree-vectorize -ftree-vectorizer-verbose=5 -ffast-math  -funsafe-loop-optimizations -ftree-loop-if-convert-stores -funsafe-math-optimizations  -Wall -Wextra -march=corei7 -msse2"
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x "+options).split("\n"))

def loop_ser():
   r"""  loop_ser.cpp
         encode( '", \\, \\t, \\n' )
   >>> loop_ser()   
   result: 0.52381 time elapsed: 0.00215188   
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x").split("\n"))


def bktree():
   r"""  bktree.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree()   
   result: 0.52381 time elapsed: 0.00215188   
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   options=" -O4  -ftree-vectorize -ftree-vectorizer-verbose=5 -ffast-math  -funsafe-loop-optimizations -ftree-loop-if-convert-stores -funsafe-math-optimizations  -Wall -Wextra -march=corei7 -msse2"
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x "+options).split("\n"))

def bktree_wool():
   r"""  bktree_wool.cpp
         encode( '", \\, \\t, \\n' )
   >>> bktree_wool()   
   result: 0.52381 time elapsed: 0.00215188   
   <BLANKLINE>
   """

   (cmd_inc,err) = Popen(["../config.sh", "--inc"], stdout=PIPE).communicate()
   (cmd_libs,err) = Popen(["../config.sh","--libso"], stdout=PIPE).communicate()
   options=" -O4  -ftree-vectorize -ftree-vectorizer-verbose=5 -ffast-math  -funsafe-loop-optimizations -ftree-loop-if-convert-stores -funsafe-math-optimizations  -Wall -Wextra -march=corei7 -msse2"
   
   print "\n".join(test_cpp(str(inspect.stack()[0][3]),cmd_inc.replace("\n",""),  cmd_libs.replace("\n","")  + " -ldl -lpthread  -lz -lm -lrt -lboost_system-mt  -lboost_thread-mt  -std=c++0x "+options).split("\n"))


if __name__ == '__main__':


    print __doc__ % {'scriptName' : sys.argv[0]}
  
    import doctest
    
    doctest.testmod(verbose=True)
    
    ''' 
    Here is the list of single tests. You can comment the previous 'doctest.testmod(verbose=True)' and uncomment
    interesting test in the list.
    '''    
#    doctest.run_docstring_examples(hello, globals(),verbose=True)
#    doctest.run_docstring_examples(levenshtein, globals(),verbose=True)
#    doctest.run_docstring_examples(fib, globals(),verbose=True)
#    doctest.run_docstring_examples(fib_ser, globals(),verbose=True)
#    doctest.run_docstring_examples(loop_wool, globals(),verbose=True)
#    doctest.run_docstring_examples(loop_ser, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree, globals(),verbose=True)
#    doctest.run_docstring_examples(bktree_wool, globals(),verbose=True)


    import os
    cmd='rm *.so'
    os.system(cmd)


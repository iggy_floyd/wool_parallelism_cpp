
/** \page
!!! WoolLoopSer
 * 
 * Example of application of Levenshtein Algorithm with the sequential approach
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>
#include <algorithm>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include <chrono>
#include <thread>

using namespace std;



const std::string test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");
  
    
    
    double wall01;
    
    std::string s1 = "igor-marfin-sdffrvvfv";
    std::string s2 = "igsSoSar-marfin-dsfewrs";
//    std::string s1 = "igor-marfin";
//    std::string s2 = "igo1-marfin";

    const size_t len1 = s1.size(), len2 = s2.size();
    
    std::vector<unsigned int> col(len2+1), prevCol(len2+1);
    for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
    wall01=tools::get_wall_time(); 
    for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;                

		for (unsigned int j = 0; j < len2; j++) {
                    col[j+1] = std::min( std::min(prevCol[1 + j] + 1, col[j] + 1),
                    prevCol[j] + (s1[i]==s2[j] ? 0 : 1) );
                }
		 col.swap(prevCol);
	}
    wall01=tools::get_wall_time()-wall01;
    double distance = prevCol[len2];
    double similarity = distance;
    if (similarity == 0 ) {      
      ss<<"similarity: "<<1.-similarity<<" time elapsed: "<<wall01;
      return ss.str();
    }
      
    double maxlen = (double) std::min(s1.size(), s2.size());  
    ss<<"result: "<<1.-std::min(1.,similarity/maxlen)<<" time elapsed: "<<wall01;    
    return ss.str();
}    



#include <boost/python.hpp>

BOOST_PYTHON_MODULE(loop_ser)
{
    using namespace boost::python;
    def("test", test);
}

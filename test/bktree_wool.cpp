

/** \page
!!! WoolBKTree
 * 
 * Example of the application of the BKTree with the Wool support.
 * 3M or 30K personal names are read to the tree.
 * 200 search operations for 'not presented in the tree' names are being done.
 * The wool parallel stealing tasks queue reduce the total time of the search by the factor of 3.
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <map>
#include  <cstring>
#include  <sstream>
#include <fstream>
#include  <locale>

#include <boost/thread/mutex.hpp>


#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"



#include "bk_tree_wool.hpp"


#include <chrono>
#include <thread>

using namespace std;






 void gen_random(char *s, const int len) {
     for (int i = 0; i < len; ++i) {
         int randomChar = rand()%(26+26+10);
         if (randomChar < 26)
             s[i] = 'a' + randomChar;
         else if (randomChar < 26+26)
             s[i] = 'A' + randomChar - 26;
         else
             s[i] = '0' + randomChar - 26 - 26;
     }
     s[len] = 0;
 }





const std::string  test()
{



std::ostringstream ss;
std::locale l("de_DE.UTF-8");

// data to be processed: 3M entries
/*
std::ifstream file("./BookerNames1M.txt");
std::ifstream file2("./BookerNames1M.txt");
std::ifstream file3("./BookerNames1M.txt");
*/

// data to be processed: 30K entries
std::ifstream file("./BookerNames10K.txt");
std::ifstream file2("./BookerNames10K.txt");
std::ifstream file3("./BookerNames10K.txt");

/*
 *  some parameters of this test
 *   
 *  rate_of_exactness --  its inverse defines how oft the property should be found in the container
 * 
 *  delay     --   in the real life,  there is a delay (it might be 10 ms or 1s or 10s  or ...) between "search calls"
 * 
 */

const double rate_of_exactness = 0.005; // only one among 200 properties should be found in the container

// assume that  we have a delay of 200 ms on average. Depends on different factors: insert time in the tree, communication with Cayley, 
// other code in the communicator.cpp,  rules (Double Booking, Overall) calculation, php background etc
size_t delay = 0; 



trees::bktree_wool<std::string, double, levenshtein_distance_operator_v2> bktree;
bktree.insert(std::string(128,'#'));

std::vector<std::string> tosearch;
std::vector<std::string> tosearch_orig;

double wall01=0.,dt_wall01= 0.,wall02=0.;

if (file.is_open() && !file.eof() ) {
    
    std::string line;     
    getline(file, line);
    wall01=tools::get_wall_time();         
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];        
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    

        bktree.insert(std::string(bb)+"-"+line+"-"+std::string(aa));
        tosearch.push_back(std::string(bb)+"-"+line+"-"+std::string(aa));
        tosearch_orig.push_back(line);
        
    }
    while (!file.eof()) {
      getline(file, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        bktree.insert(std::string(bb)+"-"+line+"-"+std::string(aa));
        tosearch.push_back(std::string(bb)+"-"+line+"-"+std::string(aa));
        tosearch_orig.push_back(line);

    }
      
    }
    file.close();
    wall01=tools::get_wall_time()-wall01;  
    std::cout<<"(1) read has been finished\n";
    std::cout<<"time elapsed: "<<wall01<<"\n";

}



if (file2.is_open() && !file2.eof() ) {
    
    std::string line;     
    getline(file2, line);
    wall01=tools::get_wall_time();         
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
        
        bktree.insert(std::string(bb)+"-"+line+"-"+std::string(aa));
        
    }    
    while (!file2.eof()) {
      getline(file2, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);   

        bktree.insert(std::string(bb)+"-"+line+"-"+std::string(aa));

    }
      
    }
    file2.close();
    wall01=tools::get_wall_time()-wall01;  
    std::cout<<"(2) read has been finished\n";
    std::cout<<"time elapsed: "<<wall01<<"\n";
    
}

if (file3.is_open() && !file3.eof() ) {
    
    std::string line;     
    getline(file3, line);
    wall01=tools::get_wall_time();         
    if (!line.empty()) {
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
        bktree.insert(std::string(bb)+"-"+line+"-"+std::string(aa));

        
    }    
    while (!file3.eof()) {
      getline(file3, line);
    if (!line.empty()) {
     
        // random string of size 10
        int sz=5;   
        char aa[sz+1];
        gen_random(aa,sz);
        char bb[sz+1];
        gen_random(bb,sz);
    
    
        bktree.insert(std::string(bb)+"-"+line+"-"+std::string(aa));


    }
      
    }
    file3.close();
    wall01=tools::get_wall_time()-wall01;         
    std::cout<<"(3) read has been finished\n";
    std::cout<<"time elapsed: "<<wall01<<"\n";
}

  
char* argv[] = { "test", "-p", "4", 0 }; //! <We set up the parameters of the command line to initilaze properly                                                  //! <Wool library
int   argc   = 3;
argc = wool_init( argc, argv );      //! <Initialization of the Wool


int count=0,count_search=0;

size_t exactness = 1/rate_of_exactness;


std::map<std::string, std::vector<std::pair<std::string,double> > > results; 

for (unsigned int i =tosearch.size()-1 ;i>=tosearch.size()-200;i--) {
    count_search++;
    std::cout<<i<<"\n";
    std::string srch = (i%(exactness) == 0)?tosearch[i]:tosearch_orig[i];
    wall01=tools::get_wall_time();         
    results[srch] = bktree.find_within(srch,1.-0.80,false,true); // get with similarity of 0.80    
    dt_wall01 += tools::get_wall_time() - wall01;
        
    
    
    // a possible delay
    std::this_thread::sleep_for(std::chrono::milliseconds(delay)); // in ms 
    
}
wool_fini( );//! <Close of the Wool      

// processing of the final results:

wall01=tools::get_wall_time();  
wall02=tools::get_wall_time(); 

for (auto& p : results) {
    if (p.second.size()>0) {
        count++;
                std::cout<<p.first<<" found to be connected to "<<p.second[0].first <<" with "<< p.second[0].second <<"\n";
    }
    else        std::cout<<p.first<<" is not connected " <<"\n";
            }


std::cout<<"\nAssumed delay between calls... "<<delay<<" ms\n";
std::cout<<"\nAll  properties stored... "<< bktree.size()<<"\n";
std::cout<<"\nTotal connected properties ... "<<count<<"\n";
std::cout<<"\nTotal time spent for "<< count_search <<" search operations... "<<dt_wall01<<" s\n";
std::cout<<"\nAverage time spent for search... "<< dt_wall01/count_search <<" s\n";
std::cout<<"\nLatency time ... "<< wall02-wall01 <<" s\n";





return ss.str();

}




#include <boost/python.hpp>

BOOST_PYTHON_MODULE(bktree_wool)
{
using namespace boost::python;
def("test", test);

}




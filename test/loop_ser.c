/** \page
 * !!! LoopStandAlone
 * 
 * This loop represents the Levenshtein algorithm using two loops
 */


#include <vector>
#include <string>
#include <algorithm>
#include <iostream>        
#include <assert.h>         

      

/**
 *  g++ -lpthread -O3 -o loop_ser loop_ser.c ../lib/libwool_parallelism_cpp.a `(cd ..; ./config.sh --inc)` -std=c++0x
 * 
*/


void * internal_loop(unsigned int i,unsigned int len2, void * prevCol, void * col, void * s1, void *s2 ) {
  std::vector<unsigned int>  & _prevCol = *(static_cast<std::vector<unsigned int>  *>(prevCol));
  std::vector<unsigned int>  &_col = *(static_cast<std::vector<unsigned int>  *>(col));
  std::string &  _s1 = *(static_cast<std::string *>(s1));
  std::string & _s2 = *(static_cast<std::string *>(s2));
   
    
   
  for (unsigned int j = 0; j < len2; j++) {
                    _col[j+1] = std::min( std::min(_prevCol[1 + j] + 1, _col[j] + 1),
                    _prevCol[j] + (_s1[i]==_s2[j] ? 0 : 1) );        
  }
    
  return prevCol;
}


int main( int argc, char **argv )
{
  
  
  std::string s1 = "igor-marfin-sdffrvvfv";
  std::string s2 = "igsSoSar-marfin-dsfewrs";
  const size_t len1 = s1.size(), len2 = s2.size();
  
  std::vector<unsigned int> col(len2+1), prevCol(len2+1);
  for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;

	for (unsigned int i = 0; i < len1; i++) {
		col[0] = i+1;                
/*
		for (unsigned int j = 0; j < len2; j++) {
                    col[j+1] = std::min( std::min(prevCol[1 + j] + 1, col[j] + 1),
                    prevCol[j] + (s1[i]==s2[j] ? 0 : 1) );
                }
		 col.swap(prevCol);
*/                
                 std::vector<unsigned int> & _prevCol = *(static_cast<std::vector<unsigned int>  *>(internal_loop(i,len2,&prevCol,&col,&s1,&s2)));
                 col.swap(_prevCol);
	}
  
  double distance = prevCol[len2];
  std::cout<<"distance: "<<distance<<"\n";
  if (distance == 0 ) {
      std::cout<<double(distance)<<"\n";
      return 0;
  }
      
  double maxlen = (double) std::min(s1.size(), s2.size());  
  std::cout<<maxlen<<"\n";
  std::cout<<std::min(1.,distance/maxlen)<<"\n";
  return 0;
  
}

/* 
 * File:   loop_wool.cpp
 * Author: Igor Marfin <Unister Gmbh 2014 > igor.marfin@unister.de
 *
 * Created on September 4, 2015, 3:52 PM
 */

 
/** \page
!!! WoolLoop 
 * 
 * Example of application of the Wool technology for Levenshtein Algorithm
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>
#include <algorithm>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"

#include <chrono>
#include <thread>

using namespace std;

#include "wool.h"

__attribute__((vector))
__attribute__((aligned(16)))
TASK_5(unsigned int, mycalc,unsigned int, _prevcol,unsigned int, _col,unsigned int, _prevcol2,char,c1,char,c2 ) {
    return std::min( std::min( _prevcol2+ 1, _col + 1),  _prevcol+ (c1==c2 ? 0 : 1) );          
    
}

__attribute__((vector))
__attribute__((aligned(16)))
LOOP_BODY_5(work, LARGE_BODY, unsigned int, j,unsigned int,i,void * , prevCol,void *,col,void *, s1, void *,s2) {
  std::vector<unsigned int>  & _prevCol = *(static_cast<std::vector<unsigned int>  *>(prevCol));
  std::vector<unsigned int>  &_col = *(static_cast<std::vector<unsigned int>  *>(col));
  std::string &  _s1 = *(static_cast<std::string *>(s1));
  std::string & _s2 = *(static_cast<std::string *>(s2));
  //_col[j+1] = std::min( std::min(_prevCol[1 + j] + 1, _col[j] + 1),_prevCol[j] + (_s1[i]==_s2[j] ? 0 : 1) );          
  _col[j+1] = CALL(mycalc,_prevCol[j],_col[j],_prevCol[1 + j],_s1[i],_s2[j]);
  
  //SPAWN(mycalc,_prevCol[j],_col[j],_prevCol[1 + j],_s1[i],_s2[j]);
  //_col[j+1] = SYNC(mycalc);
}



__attribute__((vector))
__attribute__((aligned(16)))
VOID_TASK_6(internal_loop, unsigned int, i,unsigned int, len2, void *, prevCol, void *, col, void *, s1, void *, s2  )
{
   
  FOR( work, 0, len2,i, prevCol,col,s1,s2);
  
  std::vector<unsigned int>  &  _prevCol = *(static_cast<std::vector<unsigned int>  *>(prevCol));
  std::vector<unsigned int>  & _col = *(static_cast<std::vector<unsigned int>  *>(col));
  _col.swap(_prevCol);
  
}


#pragma vector always
#pragma vector align 
#pragma ivdep

const std::string test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");
    
    double wall01;
    char* argv[] = { "test", "-p", "4", 0 }; //! <We set up the parameters of the command line to initilaze properly                                                  //! <Wool library
    int   argc   = 2;
    
    std::string str1 = "igor-marfin-sdffrvvfv";
    std::string str2 = "igsSoSar-marfin-dsfewrs";
//     std::string str1 = "igor-marfin";
//     std::string str2 = "igo1-marfin";

    const size_t len1 = str1.size(), len2 = str2.size();
    
    std::vector<unsigned int> col(len2+1), prevCol(len2+1);
    #pragma simd noassert
    for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
  
    wall01=tools::get_wall_time(); 
    argc = wool_init( argc, argv );      //! <Initialization of the Wool
    
    #pragma simd noassert
    for (unsigned int i = 0; i < len1; i++) {
	col[0] = i+1;            
        CALL(internal_loop,i,len2,&prevCol,&col,&str1,&str2 ); //! <CALL is used to start executing the 'parallel' code         
    }
           
    wool_fini( );//! <Close of the Wool
    wall01=tools::get_wall_time()-wall01;
    
    double distance = prevCol[len2];
    double similarity = distance;
    if (distance == 0 ) {      
      ss<<"similarity: "<<1.-similarity<<" time elapsed: "<<wall01;
      return ss.str();
    }
      
    double maxlen = (double) std::min(str1.size(), str2.size());  
    ss<<"result: "<<1.-std::min(1.,similarity/maxlen)<<" time elapsed: "<<wall01;    
    return ss.str();
}    



#include <boost/python.hpp>

BOOST_PYTHON_MODULE(loop_wool)
{
    using namespace boost::python;
    def("test", test);
}

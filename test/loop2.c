/** \page
 * !!! WoolLoopStandAlone2 
 * 
 * This loop represents the Levenshtein algorithm with Wool spawning tasks 
 * version 2
 */


#include <vector>
#include <string>
#include <algorithm>
#include "wool.h"
#include <iostream>        
  
TASK_5(unsigned int, mycalc,unsigned int, _prevcol,unsigned int, _col,unsigned int, _prevcol2,char,c1,char,c2 ) {
    return std::min( std::min( _prevcol2+ 1, _col + 1),  _prevcol+ (c1==c2 ? 0 : 1) );          
    
}
LOOP_BODY_5(work, LARGE_BODY, unsigned int, j,unsigned int,i,void * , prevCol,void *,col,void *, s1, void *,s2) {
  std::vector<unsigned int>  & _prevCol = *(static_cast<std::vector<unsigned int>  *>(prevCol));
  std::vector<unsigned int>  &_col = *(static_cast<std::vector<unsigned int>  *>(col));
  std::string &  _s1 = *(static_cast<std::string *>(s1));
  std::string & _s2 = *(static_cast<std::string *>(s2));
  //_col[j+1] = std::min( std::min(_prevCol[1 + j] + 1, _col[j] + 1),_prevCol[j] + (_s1[i]==_s2[j] ? 0 : 1) );          
  _col[j+1] = CALL(mycalc,_prevCol[j],_col[j],_prevCol[1 + j],_s1[i],_s2[j]);
  
  //SPAWN(mycalc,_prevCol[j],_col[j],_prevCol[1 + j],_s1[i],_s2[j]);
  //_col[j+1] = SYNC(mycalc);
}


VOID_TASK_6(internal_loop, unsigned int, i,unsigned int, len2, void *, prevCol, void *, col, void *, s1, void *, s2  )
{
   
  FOR( work, 0, len2,i, prevCol,col,s1,s2);
  
  std::vector<unsigned int>  &  _prevCol = *(static_cast<std::vector<unsigned int>  *>(prevCol));
  std::vector<unsigned int>  & _col = *(static_cast<std::vector<unsigned int>  *>(col));
  _col.swap(_prevCol);
  
}



      

/**
 *  g++ -lpthread -O3 -o loop2 loop2.c ../lib/libwool_parallelism_cpp.a `(cd ..; ./config.sh --inc)`
 * 
*/


int main( int argc, char **argv )
{
  
  argc = wool_init( argc, argv );  
  std::string str1 = "igor-marfin-sdffrvvfv";
  std::string str2 = "igsSoSar-marfin-dsfewrs";
  const size_t len1 = str1.size(), len2 = str2.size();
  
  std::vector<unsigned int> col(len2+1), prevCol(len2+1);
  for (unsigned int i = 0; i < prevCol.size(); i++)
		prevCol[i] = i;
  
    for (unsigned int i = 0; i < len1; i++) {
	col[0] = i+1;            
        CALL(internal_loop,i,len2,&prevCol,&col,&str1,&str2 );
        //SYNC(internal_loop );
    }
  

  double distance = prevCol[len2];
  std::cout<<"distance: "<<distance<<"\n";
  if (distance == 0 ) {
      std::cout<<double(distance)<<"\n";
      return 0;
  }
      
  double maxlen = (double) std::min(str1.size(), str2.size());  
  std::cout<<maxlen<<"\n";
  std::cout<<std::min(1.,distance/maxlen)<<"\n";
  wool_fini( );
  
  return 0;
  
}
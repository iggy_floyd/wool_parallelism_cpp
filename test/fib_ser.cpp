/** \page
!!! SequentialFibonacci  
 * 
 * Example of the fibonacci algorithm using sequential approach
 */


#include  <iostream>
#include  <fstream>
#include  <string>
#include  <vector>
#include  <cstring>
#include  <sstream>
#include  <locale>



/**
 * tools to measure the time of the execution.
 */
#include "tools/cpu-time.hpp"
#include "tools/vprintf.hpp"




#include <chrono>
#include <thread>

using namespace std;

/*!
 * realization of the fibonacci algorithm using the recurrency
 */
int fib (int n)
{
    if (n<2) return (n);
    else
    {
        int x, y;
        x = fib (n-1);
        y = fib (n-2);
        return (x+y);
    }
}


const std::string  test()
{
    std::ostringstream ss;
    std::locale l("de_DE.UTF-8");
  
    
    int m, n=40;
    double wall01;
    wall01=tools::get_wall_time();   
    m = fib (n);
    wall01=tools::get_wall_time()-wall01;   
    ss<<"result: "<<m<<" time elapsed: "<<wall01;    
    return ss.str();    
}



#include <boost/python.hpp>

BOOST_PYTHON_MODULE(fib_ser)
{
    using namespace boost::python;
    def("test", test);

}

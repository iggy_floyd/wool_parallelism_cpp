
/** \page Helper tools
!!!  Helper tools
 * 
 * !!The list of tools includes 
 * - functions to measure the CPU time of the executions of C/C++ code
 * - transformators of the text strings
 * - a modified version of the printf
 * - a generator of the random strings
 *   
 */


/** \dir
!!! 
 * 
 * The tools directory contains helper functions.
 */

#ifndef CPUTIME_H
#define CPUTIME_H

#include <sys/time.h>
#include <time.h>
#include <unistd.h>


namespace tools {
    /*!
     * returns the current Wall time.
     * 
     */
	double get_wall_time(){
	    struct timeval time;
	    if (gettimeofday(&time,NULL)){
	        //  Handle error
	        return 0;
	    }
	    return (double)time.tv_sec + (double)time.tv_usec * .000001;
}
    /*!
     * returns the cpu time
     * 
     */
	double get_cpu_time(){
	    return (double)clock()/CLOCKS_PER_SEC;
	}


}


#endif

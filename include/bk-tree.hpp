
/** \page
!!! BKTreeDefinition
 * 
 * BKTree class
 */



#ifndef _BKTREE_HPP_
#define _BKTREE_HPP_


#include <fstream>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <cmath>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>


/****** BK-TREE/bktree/boost::serialization
 * DESCRIPTION 
 *    inclusion of the boost::serialization
 * SOURCE
*/


#include "tools/random_transformation_string.h"

#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>

/******/

/****** BK-TREE/bktree/Levenshtein
 * DESCRIPTION 
 *    Levenshtein Distance Algorithm
 * SOURCE
*/


#include "levenshtein-distance.hpp"
#include <numeric>

/******/




namespace trees {

namespace bktree_detail {

    
     void gen_random(char *s, const int len) {
     for (int i = 0; i < len; ++i) {
         int randomChar = rand()%(26+26+10);
         if (randomChar < 26)
             s[i] = 'a' + randomChar;
         else if (randomChar < 26+26)
             s[i] = 'A' + randomChar - 26;
         else
             s[i] = '0' + randomChar - 26 - 26;
     }
     s[len] = 0;
 }
     
 
     std::string random_string( size_t length )
{
    auto randchar = []() -> char
    {
        const char charset[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        //"abcdefghijklmnopqrstuvwxyz+#<!%&/*~";
        "abcdefghijklmnopqrstuvwxyz";
        const size_t max_index = (sizeof(charset) - 1);
        return charset[ rand() % max_index ];
    };
    std::string str(length,0);
    std::generate_n( str.begin(), length, randchar );
    return str;
}
     
     
    

/****c* BK-TREE/bktree/trees::bktree_detail::tree_node
 * DESCRIPTION 
 *  A node definition for the bktree
 * |html <img src="figs/classtrees_1_1bktree__detail_1_1tree__node__coll__graph.png">
 * SOURCE
*/


template <typename KeyType, typename MetricType, typename Distance>
class tree_node
{
private:
	typedef tree_node<KeyType, MetricType, Distance> NodeType;
	
private:
	KeyType value;
	std::map<MetricType, NodeType *> *children;
        bool not_erase_map;
        

public:
	tree_node(const KeyType &key)
		: value(key), children(NULL),counter(1),not_erase_map(false) { }
	tree_node()
                : value(""), children(NULL),counter(1),not_erase_map(false) { }


	~tree_node() {
		if (children) {
                         
                        if (!not_erase_map) {
			for (auto iter = children->begin(); iter != children->end(); ++iter)
				delete iter->second;
                        }
			delete children;
		}
	}

         size_t depth = 0;
         static size_t maxdepth;
         size_t numvisit=0;
         bool active = false;

private:
	friend class boost::serialization::access;
	template<class Archive>
                void serialize(Archive & ar, const unsigned int version)
                {
			ar &  children;
			ar &  value;
			ar &  counter;
                        ar &  not_erase_map;
                        ar &  depth;
                        ar &  maxdepth;
                        ar &  numvisit;
                        ar &  active;
                }



public: 
		KeyType getVal() const  { return value;}
		int counter; //to store the number of the different entries with the same key. It is needed by deletion.


	bool insert(NodeType *node,int _depth=0) {
		if (!node)
			return false;



		Distance d;
		MetricType distance = d(node->value, this->value);
		if (distance < 1e-6) {
                
                 if (this->active) {
		   this->counter++;                    
		   return false; /* value already exists */
                 
                 } else {
                     
                   this->value = node->value;
                   this->active = true; 
		   return false; /* randomvalue already exists */                     
                 }
		}

                node->active = true;

		if (!children) {
			children = new std::map<MetricType, NodeType *>();
                        this->depth=_depth+1;
                        if (depth > maxdepth) maxdepth = depth;
                }
                
                if (this->depth>1) this->numvisit++;
                
		auto iterator = children->find(distance);
		if (iterator == children->end()) {
			children->insert(std::make_pair(distance, node));                        
			return true;
		}

                
                if (iterator->second->numvisit>100 ) {
                    
                    std::string strrandom= tools::random_transformation_string(iterator->second->value,0.20).generate();
                    MetricType distancerandom = d(strrandom, this->value);
                    auto iteratorrandom = children->find(distancerandom);
                    if (iteratorrandom == children->end()) {
                        
                        children->insert(std::make_pair(distancerandom, new NodeType(strrandom))); 
                        iterator->second->numvisit = 0;
                    }
                }
                
		return iterator->second->insert(node,depth);
	}




public: 
        
          bool remove(const KeyType &key) {
              std::cout<<"...removing "<< key <<" ...\n";              
              std::cout<<"...this "<< this <<" ...\n";       
              std::cout<<"...in"<< this->getVal() <<" ...\n";  
	      std::vector<NodeType *> a = _find2erase(key,this);
              std::cout<<"...found "<< a.size() <<" ...\n";
                if (a.size()>1) {
                    
                        std::cout<<"..I am "<<this->getVal()<<"\n";
			Distance d;
	                MetricType distance = d(a[1]->getVal(), this->value);
                        std::cout<<"...checking the exactness...\n";
                        std::cout<<"...checking the exactness...distance "<<distance<<"\n";
			if (distance == 0) return true;
                        std::cout<<"...passed...\n";
			distance = d(a[0]->getVal(), a[1]->getVal());
                        
                        // the new code begin
                        
                        if (a[1]->has_children()) {
                            std::cout<<"...find substitute...\n";
                            auto substitute = a[1]->children->begin();
                            auto it = a[1]->children->begin();
                            std::advance(it,1);
                            std::cout<<"...advance...\n";
                            
                            while (it!= a[1]->children->end()) {
                                std::cout<<"...new value..."<<it->second->getVal()<<"\n";
                                substitute->second->insert(new NodeType(it->second->getVal()));
                                std::vector<KeyType>  childnames;
                                it->second->_find_all_children(childnames);
                                std::cout<<"...new size..."<<childnames.size()<<"\n";
                                std::for_each(childnames.begin(), childnames.end(),
                                        [&substitute] (KeyType &p) { substitute->second->insert(new NodeType(p)); });
                                
                                        
                                std::cout<<"...deleting ..."<<it->second->value<<"\n";        
                                delete it->second;                                
                                it=a[1]->children->erase(it);
                                std::cout<<"...erased ..."<<"\n";        
                                                                
                            }

                            std::cout<<"...true...a[0]...name::"<<a[0]->value<<"\n";
                            std::cout<<"...true...a[1]...name::"<<a[1]->value<<"\n";
                            a[0]->children->find(distance)->second = substitute->second;                                                       
                            a[1]->not_erase_map=true;
                            delete a[1];
                            
                            
                        } else { 
                        
                            std::cout<<"...false...a[0]...name::"<<a[0]->value<<"\n";
                            std::cout<<"...false...a[1]...name::"<<a[1]->value<<"\n";
                            
                            a[0]->children->erase(distance);
                            delete a[1];
                        }
              
                    // the new code end
                    
                        // the old code
              
                        /*
                        a[0]->children->erase(distance);
			if (a[1]->has_children()) {
                        std::vector<KeyType>  childnames;
                        a[1]->_find_all_children(childnames);
                         std::cout<<"total number of children..."<< childnames.size() <<"\n";
	                for (auto iter = a[1]->children->begin(); iter != a[1]->children->end(); ++iter)  			
			{
                                std::cout<<"recalculation..."<< iter->second->getVal() <<"\n";	
                                std::cout<<"with children..."<< iter->second->children->size() <<"\n";	
	        	        //insert(new NodeType(iter->second->getVal()));
                                insert(iter->second);
			}
                        }
                        */
			//delete a[1];
                        std::cout<<"...this afterwards..."<<this->value<<"\n";
			return true;
                         
		}
              
		return false;
}

protected:
      std::vector<NodeType *> _find2erase(const KeyType &key, NodeType * prev)
	{
                Distance d;
                std::cout<<"checking ..."<<this->value<<"\n";
                MetricType distance = d(key, this->value);
                if (distance < 1e-6) {
                        std::vector<NodeType *>a;
                        this->counter--;
                         std::cout<<"bk-tree  to delete    ::"<<this->value <<"::"<<this->counter<<"\n";
//                    if (key == this->value) {
                         if (this->counter == 0) {
                            a.push_back(prev); a.push_back(this);
                               std::cout<<"bk-tree  delete    ::"<<this->value <<"::"<<"\n";
                         } else   std::cout<<"bk-tree cannot delete because it's doubled   ::"<< this->value <<"::"<<"\n";
                        return a;
//                    } else return a; // return empty in order preserve a node from removing

                }

		if (!children) { return std::vector<NodeType *>();}

                std::cout<<"going to iterators ..."<<"\n";
                auto iterator = children->find(distance);
		if (iterator != children->end())   { std::cout<<"found iterator ..."<<iterator->second->value<<"\n";    return iterator->second->_find2erase(key,this); }
                else { return std::vector<NodeType *>();}
	}

protected:
         void _find_all_children( std::vector<KeyType> & result) {
                       
             
            if (this->has_children())
			for (auto iter = children->begin(); iter != children->end(); ++iter) {
				result.push_back(iter->second->getVal());
				iter->second->_find_all_children(result);
			}
        }
      

protected:
	bool has_children() const {
		return this->children && this->children->size();
	}


protected:

    // to test: as a step of bk-tree optimization of the search
	//void _find_within(std::vector<std::pair<KeyType, MetricType> > &result, const KeyType &key, MetricType d, bool op, bool use_first_result ) const {
	bool _find_within(std::vector<std::pair<KeyType, MetricType> > &result, const KeyType &key, MetricType d, bool op, bool use_first_result ) const {


 if (use_first_result && result.size()>0) return true ;

	    Distance f;
		MetricType n = f(key, this->value);
                if (op) {
	          	if (n <= d)
				result.push_back(std::make_pair(this->value, n));
		} else {
			if (n <= d) {
                                result.push_back(std::make_pair(this->value, 1-n));
			}
		}




    // to test: as a step of bk-tree optimization of the search
	//if (use_first_result && result.size()>0) return;
    if (use_first_result && result.size()>0) return true ;


		if (!this->has_children())
// to test: as a step of bk-tree optimization of the search
//			return;
        return false;

		for (auto iter = children->begin(); iter != children->end(); ++iter) {
                    if (!iter->second->active) continue;
			MetricType distance = iter->first;
			if (n - d <= distance && distance <= n + d) {

// to test: as a step of bk-tree optimization of the search
//				iter->second->_find_within(result, key, d,op,use_first_result);
                bool boolres=iter->second->_find_within(result, key, d,op,use_first_result);
                if (boolres) return true;
            }

		}


// to test: as a step of bk-tree optimization of the search
//        return;
        return false;
	}


public:

	std::vector<std::pair<KeyType, MetricType> > find_within(const KeyType &key, MetricType d, bool op,bool use_first_result) const {
		std::vector<std::pair<KeyType, MetricType> > result;


		_find_within(result, key, d,op,use_first_result);


		return result;
	}



public:
	void dump_tree(int depth = 0) {
		for (int i = 0; i < depth; ++i)
			std::cout << "    ";
		std::cout << this->value << std::endl;
		if (this->has_children())
			for (auto iter = children->begin(); iter != children->end(); ++iter) {
                             if (!iter->second->active) continue;
				std::cout << " children with depth " << depth << " of " << this->value << " (distance: "<< iter->first  <<")";
				iter->second->dump_tree(depth + 1);
			}
	}




}; /* tree_node */


template <
        typename KeyType, 
        typename MetricType, 
        typename Distance
>
size_t tree_node<KeyType, MetricType, Distance>::maxdepth = 0;

/******/

/****c* BK-TREE/bktree/trees::bktree
 * DESCRIPTION 
 *   bktree definition
 * |html <img src="figs/classtrees_1_1bktree__coll__graph.png">
 * SOURCE
*/

	template <
		typename KeyType,
		typename MetricType
	>
	struct default_distance
	{
		MetricType operator()(const KeyType &ki, const KeyType &kj) {
			return sqrt((ki - kj) * (ki - kj));
		}
	};



} /* namespace bktree_detail */



	template <
		typename KeyType,
		typename MetricType = double,
		typename Distance = bktree_detail::default_distance<KeyType, MetricType>
	>
	class bktree
	{

            private:
		typedef bktree_detail::tree_node<KeyType, MetricType, Distance> NodeType;		
                NodeType *  m_top;
		size_t m_n_nodes;
                

            public:                
		bktree() : m_top(nullptr), m_n_nodes(0) { }

                


	private:
        	friend class boost::serialization::access;
	        template<class Archive>
        	void serialize(Archive & ar, const unsigned int version)
                {
                        ar &  m_top; 
                        ar & m_n_nodes; 
                }



	public:
		void insert(const KeyType &key) {
			NodeType *node = new NodeType(key);
			if (m_top == nullptr) {
				m_top = node;
				m_n_nodes = 1;
                                
				return;
			}
			if (m_top->insert(node))	++m_n_nodes;
			else delete node;

			return;
		}

		bool remove( const KeyType &key)
		{	
			if (!m_top) return false;
			else {
                            std::cout<<"...remove m_top: "<<m_top<<"\n";
                            std::cout<<"...remove m_top value: "<<m_top->getVal()<<"\n";
				if (m_top->remove(key)) { --m_n_nodes;    std::cout<<"...remove m_top final: "<<m_top<<"\n"; }
                                else return false;

			}

			return true;
		}

        public: 
                    void randomization() {
                
                        NodeType::maxdepth = 0;

                        if (m_top == nullptr) return;
                        
                        //size_t lenmax = 6+6+10+6+3;
                        //size_t lenmax = 10+10+10+6+3;
                        size_t lenmax = 4+4+4+6+3;
                        //size_t lenmax = 15;
                        
                        size_t lenmin = 2+2+6+6+3;
                      while (NodeType::maxdepth <3) {  
                      //while (NodeType::maxdepth <6) {  
                      //   while (NodeType::maxdepth <10) {  
                      //  while (NodeType::maxdepth <15) {  
                        //while (NodeType::maxdepth <18) {  
                        //while (NodeType::maxdepth <19) {  
                        //while (NodeType::maxdepth <30) {  
                        

                            size_t len= lenmin + rand() % (lenmax-lenmin);
                            
                            //insert(trees::bktree_detail::random_string(len));
                            //std::cout<<"maxdeph..."<<NodeType::maxdepth<<"\n";
                            NodeType *node = new NodeType(trees::bktree_detail::random_string(lenmax));
                            //NodeType *node = new NodeType(trees::bktree_detail::random_string(len));
                            if (!m_top->insert(node)) {delete node; std::cout<<"something wrong with randomization\n";}
                            

                            
                            
                        }
                
                
                    }
        
                
		std::vector<std::pair<KeyType, MetricType>> find_within(KeyType key, MetricType d, bool op = true, bool use_first_result=false) const {
			return m_top->find_within(key, d, op,use_first_result);
		}

		size_t size() const {
			return m_n_nodes;
		}


		void dump_tree() {
			m_top->dump_tree();
		}

                
                size_t getMaxDepth() {
                    return NodeType::maxdepth;
                }


};  /*  bktree  */
/******/

} /* namespace qq */




#endif

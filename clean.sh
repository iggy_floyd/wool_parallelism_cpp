#!/bin/bash 

find `pwd` -iname "*.o" | sort -h -r | xargs -I {} rm {} | bash
find `pwd` -iname "*.so" | sort -h -r | xargs -I {} rm {} | bash
find `pwd` -iname "lib*a" | sort -h -r | xargs -I {} rm {} | bash

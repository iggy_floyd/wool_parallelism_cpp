#!/bin/bash

  program=`basename \`pwd\``  

  wool_obj=`find ./ -iname "*wool.o" | grep  '\/wool.o'`

  for i in src/*.cc; do g++ -fPIC -std=c++0x  -g -Iinclude  -c ${i}; done
  ar cru lib${program}.a *.o `echo $wool_obj`
  ranlib lib${program}.a
  mv  lib${program}.a lib
  rm *.o 

